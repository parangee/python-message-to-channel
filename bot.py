from discord.ext import commands
import discord
import os

from dotenv import load_dotenv

load_dotenv()

client = commands.Bot(command_prefix='!')


class SendMessage(commands.Cog):
    @commands.has_permissions(manage_guild=True)
    @commands.command(name="메시지")
    async def send_message(self, ctx: commands.Context, channel: discord.TextChannel, *message):
        await channel.send(' '.join(message))

    @commands.command(name="dm")
    async def dm(seld, ctx, mem:discord.User, *msg):
        await mem.send(' '.join(msg))

    @commands.Cog.listener()
    async def on_ready(self):
        print('ready')


client.add_cog(SendMessage())


client.run(os.getenv('TOKEN'))
